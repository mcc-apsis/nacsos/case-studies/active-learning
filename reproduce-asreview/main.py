from functools import partial

from asreview.models.balance import DoubleBalance
from modAL.uncertainty import uncertainty_sampling, entropy_sampling, margin_sampling
import numpy as np
import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.naive_bayes import MultinomialNB, ComplementNB

import os
import sys

sys.path.insert(1, os.path.join(sys.path[0], ".."))  # **** python modules
from helpers import random_sampling_strategy, no_balance


def max_query_strategy(classifier, X, n_instances=1, **kwargs):
    predictions = classifier.predict_proba(X)
    query_indices = np.argsort(predictions[:, 0])[:n_instances]
    return query_indices


def tfidf(X):
    X = TfidfVectorizer(ngram_range=(1, 2), stop_words="english").fit_transform(
        X.values.astype("U")
    )
    return X.toarray()


def read_data():
    dfs = []
    ace = pd.read_csv("data/ace.csv")
    ace = pd.DataFrame({"X": ace["abstract"], "y": ace["label_included"]})
    dfs.append(("ace", ace))
    for name in ["hall", "ptsd", "virus"]:
        df = pd.read_csv(f"data/{name}.csv")
        df = pd.DataFrame({"X": df["abstract"], "y": df["included"]})
        dfs.append((name, df))
    return dfs


datasets = read_data()
datasets = np.array(datasets)[[3]]
TEST_SIZE = 0.2
TRANSFORMERS_N_EPOCHS = 8
TRANSFORMERS_BALANCED = True
classifiers = [
    ("naive bayes", tfidf, partial(MultinomialNB, alpha=2.1292734234216257)),
    ("naive bayes compl", tfidf, partial(ComplementNB)),
]
query_strategies = [
    ("random sampling", random_sampling_strategy),
    ("uncertainty sampling", uncertainty_sampling),
    ("entropy sampling", entropy_sampling),
    ("margin sampling", margin_sampling),
][:1]
N_STEPS = 3
N_RUNS = 1
FOLDER = "output"


def double_balance(X, y):
    balancer = DoubleBalance(
        a=0.7782141491367417, alpha=1.39537927110139, b=0.9147018292161516
    )
    return balancer.sample(
        np.array(X),
        np.array(y),
        train_idx=np.array(range(len(X))),
        shared=None,
    )


balancers = [
    ("double balance", double_balance),
    ("no balance", no_balance),
]

if __name__ == "__main__":
    from experiments import evaluate
    from visual import visualize

    TRAIN = True
    RESUME = False
    if TRAIN:
        df = evaluate(
            datasets=datasets,
            classifiers=classifiers,
            query_strategies=query_strategies,
            test_size=TEST_SIZE,
            n_runs=N_RUNS,
            n_steps=N_STEPS,
            resume=RESUME,
            folder=FOLDER,
            balancers=balancers,
        )
    else:
        df = pd.read_csv(f"{FOLDER}/evaluation.csv")
    visualize(df, FOLDER)
