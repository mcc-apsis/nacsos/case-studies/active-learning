from functools import partial
from importlib import reload

from modAL.uncertainty import uncertainty_sampling, entropy_sampling, margin_sampling
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.naive_bayes import ComplementNB
from sklearn.svm import SVC
from small_text.classifiers.factories import SklearnClassifierFactory

from helpers import no_balance, tfidf, random_sampling_strategy
from read_data import read_data
import transformer_models

reload(transformer_models)
from transformer_models import get_abstracts, TransformerEstimator

datasets = read_data()[3:10]
TEST_SIZE = 0.4
TRANSFORMERS_N_EPOCHS = 8
TRANSFORMERS_BALANCED = True
classifiers = [
    (
        "SVM",
        tfidf,
        partial(SVC, kernel="linear", class_weight="balanced", probability=True),
    ),
    (
        "logistic regression",
        tfidf,
        partial(LogisticRegression, solver="liblinear"),
    ),
    ("naive bayes", tfidf, ComplementNB),
    ("random forest", tfidf, RandomForestClassifier),
    (
        "DistilRoBERTa",
        get_abstracts,
        partial(
            TransformerEstimator,
            checkpoint="distilroberta-base",
            epochs=TRANSFORMERS_N_EPOCHS,
            balanced=TRANSFORMERS_BALANCED,
        ),
    ),
    (
        "ClimateBERT",
        get_abstracts,
        partial(
            TransformerEstimator,
            checkpoint="climatebert/distilroberta-base-climate-f",
            epochs=TRANSFORMERS_N_EPOCHS,
            balanced=TRANSFORMERS_BALANCED,
        ),
    ),
    (
        "SciBERT",
        get_abstracts,
        partial(
            TransformerEstimator,
            checkpoint="allenai/scibert_scivocab_uncased",
            epochs=TRANSFORMERS_N_EPOCHS,
            balanced=TRANSFORMERS_BALANCED,
        ),
    ),
][:-3]
query_strategies = [
    ("random sampling", random_sampling_strategy),
    ("uncertainty sampling", uncertainty_sampling),
    ("entropy sampling", entropy_sampling),
    ("margin sampling", margin_sampling),
][:2]
balancers = [("no balance", no_balance)]
N_STEPS = 50
N_RUNS = 3

RESUME = True
FOLDER = "output-50"

if __name__ == "__main__":
    from experiments import evaluate
    from visual import visualize

    df = evaluate(
        datasets=datasets,
        classifiers=classifiers,
        query_strategies=query_strategies,
        balancers=balancers,
        test_size=TEST_SIZE,
        n_runs=N_RUNS,
        n_steps=N_STEPS,
        resume=RESUME,
        folder=FOLDER,
    )
    print(df.to_string())
    visualize(df, FOLDER)
