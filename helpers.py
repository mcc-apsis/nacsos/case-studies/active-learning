import copy
import nltk
from nltk.stem import PorterStemmer
import numpy as np
import re
from sklearn.feature_extraction.text import TfidfVectorizer

try:
    nltk.data.find("corpora/stopwords")
except LookupError:
    nltk.download("stopwords")
from nltk.corpus import stopwords


def pretty(n):
    return float(f"{n:.2f}")


def random_sample(array, k):
    brray = copy.deepcopy(array)
    np.random.shuffle(brray)
    sample = brray[:k]
    return np.array(sample)


def tfidf(X):
    stemmer = PorterStemmer()
    words = stopwords.words("english")
    cleaned_abstracts = X.apply(
        lambda x: " ".join(
            [
                stemmer.stem(i)
                for i in re.sub("[^a-zA-Z]", " ", x).split()
                if i not in words
            ]
        ).lower()
    )
    X = TfidfVectorizer(
        min_df=3, stop_words="english", sublinear_tf=True, norm="l2", ngram_range=(1, 2)
    ).fit_transform(cleaned_abstracts)
    return X.toarray()


def random_sampling_strategy(classifier, X, n_instances=1, **kwargs):
    idx = list(range(len(X)))
    np.random.shuffle(idx)
    sample = idx[:n_instances]
    return np.array(sample)


def no_balance(X, y):
    return X, y
