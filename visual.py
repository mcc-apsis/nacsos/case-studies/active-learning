# visualizes the results of `experiments.py`

from functools import partial
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns

classifier_order = [
    "random forest",
    "logistic regression",
    "naive bayes",
    "SVM",
    "DistilRoBERTa",
    "ClimateBERT",
]


def visualize(df, folder="output"):
    figs = []

    for metric in [
        "f1",
        "precision",
        "recall",
        "relevant_seen_or_predicted_relative",
        "work_saved_within_target_recall",
        "train_all_f1",
        "train_all_precision",
        "train_all_recall",
    ]:
        sns.set(font_scale=0.7)
        fig = sns.relplot(
            data=df,
            x="seen_relative",
            y=metric,
            col="classifier",
            row="dataset",
            hue="query_strategy",
            style="balancer",
            kind="line",
            # col_order=classifier_order,
        )
        figs.append(fig)
        plt.savefig(f"{folder}/progress_{metric}.png")
        plt.close()

    def work_saved(group, target_recall):
        return float(group["work_saved_within_target_recall"].max())

    TARGET_RECALL = 0.95
    work_saved_df = (
        df.groupby(["query_strategy", "classifier", "dataset", "run"])  # type: ignore
        .apply(partial(work_saved, target_recall=TARGET_RECALL))
        .reset_index(name="max_work_saved_within_target_recall")
    )
    fig = sns.boxplot(
        data=work_saved_df,
        x="classifier",
        y="max_work_saved_within_target_recall",
        hue="query_strategy",
        order=classifier_order,
    )
    fig.set_title(f"with target recall {TARGET_RECALL}")
    figs.append(fig)
    plt.savefig(folder + "/work_saved.png")
    return figs


if __name__ == "__main__":
    folder = "output-50"
    df = pd.read_csv(f"{folder}/evaluation.csv")
    # print(df.to_string())
    visualize(df, folder=folder)
