import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns

df = pd.DataFrame()
for i in [5, 10, 20, 50]:
    df = pd.concat([df, pd.read_csv(f"output-{i}/evaluation.csv")], ignore_index=True)
df.to_csv("output-all/evaluation.csv", index=False)
parameters = ["query_strategy", "classifier", "dataset", "run", "n_steps"]
work_saved_df = (
    df.groupby(parameters)  # type: ignore
    .apply(lambda group: float(group["work_saved_within_target_recall"].max()))
    .reset_index(name="max_work_saved_within_target_recall")
)

# print(df.columns)
# print(work_saved_df)


sns.relplot(
    data=df,
    x="seen",
    y="f1",
    # col=parameters,
    hue="query_strategy",
    style="query_strategy",
    kind="scatter",
)
plt.savefig("output-all/seen.png")
# no interesting results
