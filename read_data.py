import os
import pandas as pd
import numpy as np
import xml.etree.ElementTree as ET

from helpers import pretty

# Copied from:
# - https://github.com/mcallaghan/rapid-screening/blob/master/analysis/rapid_review.py
# - https://github.com/mcallaghan/rapid-screening/blob/master/analysis/run_scenarios.py


def read_data():  # -> list[tuple[str,pd.DataFrame]]:
    def get_field(a, f):
        try:
            f = list(a.iter(f))[0]
            if f and f.text is None:
                try:
                    return list(f.iter("style"))[0].text
                except:
                    pass
            return f.text
        except:
            return None

    def get_mesh_headings(a):
        ms = []
        for m in a.iter("MeshHeading"):
            d = m.find("DescriptorName")
            if d is not None:
                ms.append(f"MESHHEAD{d.attrib['UI']}")
        return " " + " ".join(ms)

    def parse_pmxml(path):
        tree = ET.parse(path)
        root = tree.getroot()
        docs = []
        for a in root.iter("PubmedArticle"):
            docs.append(
                {
                    "ab": get_field(a, "AbstractText"),
                    "PMID": get_field(a, "PMID"),
                    "ti": get_field(a, "ArticleTitle"),
                    "mesh": get_mesh_headings(a),
                }
            )
        df = pd.DataFrame.from_dict(docs)
        df["PMID"] = pd.to_numeric(df["PMID"])
        return df

    document_index = None

    dir = os.path.dirname(__file__)
    for fpath in os.listdir(f"{dir}/data/"):
        if "cohen_all" in fpath:
            ndf = parse_pmxml(f"{dir}/data/{fpath}")
            if document_index is None:
                document_index = ndf
            else:
                document_index = pd.concat([document_index, ndf])

    document_index = document_index.drop_duplicates()

    cohen_db = pd.read_csv(
        f"{dir}/data/epc-ir.clean.tsv",
        sep="\t",
        header=None,
        names=["review", "EID", "PMID", "ab_relevant", "fulltext_relevant"],
    )

    cohen_db["relevant"] = np.where(cohen_db["ab_relevant"] == "I", 1, 0)
    cohen_db = cohen_db[["review", "PMID", "relevant"]]

    dfs = []
    for name, group in cohen_db.groupby("review"):
        df = pd.merge(
            group,
            document_index,
        )
        if df.shape[0] > 1000000:
            continue
        df = df.dropna().reset_index(drop=True)
        # df.to_csv(f"data/{name}.csv")
        df["X"] = df["ab"]
        df["y"] = df["relevant"]
        dfs.append((name, df))

    # nets_df = pd.read_csv("data/with_abstract.csv")
    # nets_df.rename(columns={"Relevant": "relevant", "content": "ab"}, inplace=True)
    # dfs.append(("NegativeEmissionsTechnologies (partial)", nets_df))

    return list(sorted(dfs, key=lambda x: len(x[1])))


if __name__ == "__main__":
    dfs = read_data()
    metadata = [
        {
            "name": name,
            "size": len(df),
            "relevant": len(df[df["relevant"] == True]),
            "relevant proportion": pretty(len(df[df["relevant"] == True]) / len(df)),
        }
        for name, df in dfs
    ]
    meta_df = pd.DataFrame(metadata)
    meta_df.to_csv("datasets.csv", index=False)
