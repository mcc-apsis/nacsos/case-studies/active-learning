from functools import partial
import gc
import itertools as it
from pprint import pformat
import random
from time import time

import numpy as np
import pandas as pd
from sklearn.metrics import recall_score, precision_score, f1_score
from sklearn.model_selection import train_test_split
from small_text.data import SklearnDataset
import torch

from helpers import pretty, random_sampling_strategy

random.seed(3785294)


def evaluate(
    datasets,
    classifiers,
    query_strategies,
    balancers,
    test_size,
    n_runs,
    n_steps,
    resume,
    folder,
):
    if resume:
        evaluation_df = pd.read_csv(f"{folder}/evaluation.csv")
    else:
        evaluation_df = pd.DataFrame()
    combinations = list(it.product(datasets, classifiers, query_strategies, balancers))
    for i_run in range(1, n_runs + 1):
        random_state = random.randint(0, 2**32 - 1)
        for i_combination, (
            (df_name, full_df),
            (classifier_name, representation, classifier),
            (query_strategy_name, query_strategy),
            (balancer_name, balancer),
        ) in enumerate(combinations, 1):
            keywords = pformat(
                classifier.keywords if isinstance(classifier, partial) else {}
            )
            print(
                f"evaluating combination {i_combination} of {len(combinations)}, run {i_run} of {n_runs} ({pretty(i_combination / len(combinations) * 100)}%)"
            )
            if len(evaluation_df) > 0:
                h = evaluation_df
                cached = h[
                    (h.dataset == df_name)
                    & (h.test_size == test_size)
                    & (h.classifier == classifier_name)
                    & (h.keywords == keywords)
                    & (h.query_strategy == query_strategy_name)
                    & (h.balancer == balancer_name)
                    & (h.run == i_run)
                    & (h.n_steps == n_steps)
                ]
                if len(cached) == n_steps:
                    print(f" - cached")
                    continue
            # x = list(representation(full_df))
            # y = full_df["y"]
            # dataset = SklearnDataset(x, y)
            df = pd.DataFrame(
                {
                    "X": list(representation(full_df["X"])),
                    "y": full_df["y"],
                    "seen": [False] * len(full_df),
                }
            )
            train_idx, test_idx = train_test_split(
                df.index, test_size=test_size, random_state=random_state
            )
            test_df = df.loc[test_idx]
            df = df.loc[train_idx]
            clf = None
            # query pools and successively train the classifier
            for i_step in range(1, n_steps + 1):
                print(f"step {i_step} of {n_steps}")
                n = round(len(df) / n_steps * i_step) - len(df[df["seen"] == True])
                t0 = time()
                if clf is None:
                    # initialization phase
                    # choose random samples to initialize the classifier
                    start_idx = random_sampling_strategy(
                        classifier, df[df["seen"] == False]["X"], n
                    )
                    start_idx_ = [df[df["seen"] == False].index[x] for x in start_idx]
                    df.loc[start_idx_, "seen"] = True
                    # make sure that some but not all samples are of the relevant class
                    # otherwise wait for more samples in the next iteration
                    seen_and_relevant = df[(df["seen"] == True) & (df["y"] == 1)]
                    if 0 < len(seen_and_relevant) < len(df[df["seen"] == True]):
                        # initialize the classifier
                        clf = classifier()
                else:
                    # active learning phase
                    query_idx = query_strategy(
                        clf, list(df[df["seen"] == False]["X"]), n
                    )
                    if query_idx.ndim == 2:
                        # for some reason this data is not always in the right shape, so we fix this:
                        query_idx = query_idx[0]
                    query_idx_ = [df[df["seen"] == False].index[x] for x in query_idx]
                    df.loc[query_idx_, "seen"] = True
                if clf is not None:
                    X_bal, y_bal = balancer(
                        df[df["seen"] == True]["X"], df[df["seen"] == True]["y"]
                    )
                    clf.fit(list(X_bal), y_bal)
                    # evaluation
                    training_time = time() - t0
                    t1 = time()
                    y_train_pred = clf.predict(list(df[df["seen"] == True]["X"]))
                    y_train_true = df[df["seen"] == True]["y"]
                    y_train_all_pred = clf.predict(list(df["X"]))
                    y_train_all_true = df["y"]
                    y_test_pred = clf.predict(list(test_df["X"]))
                    y_test_true = test_df["y"]
                    unseen_index = np.array(df[df["seen"] == False].index)
                    relevant_seen = len(df[(df["seen"] == True) & (df["y"] == 1)])
                    relevant_seen_rel = relevant_seen / len(df[df["y"] == 1])
                    relevant_unseen = len(df[(df["seen"] == False) & (df["y"] == 1)])
                    if i_step < n_steps:
                        y_unseen_pred = clf.predict(list(df.loc[unseen_index, "X"]))
                        y_unseen_true = df.loc[unseen_index, "y"]
                        relevant_seen_or_predicted_rel = (
                            relevant_seen
                            + recall_score(
                                y_unseen_true, y_unseen_pred, zero_division=0  # type: ignore
                            )
                            * relevant_unseen
                        ) / len(df[df["y"] == 1])
                        work_saved = len([a for a in y_unseen_pred if a == 0]) / len(df)
                    else:
                        relevant_seen_or_predicted_rel = 1
                        work_saved = 0
                    TARGET_RECALL = 0.95
                    evaluation = {
                        "dataset": df_name,
                        "test_size": test_size,
                        "classifier": classifier_name,
                        "keywords": keywords,
                        "query_strategy": query_strategy_name,
                        "balancer": balancer_name,
                        "run": i_run,
                        "n_runs": n_runs,
                        "step": i_step,
                        "n_steps": n_steps,
                        "seen": len(df[df["seen"] == True]),
                        "seen_relative": pretty(len(df[df["seen"] == True]) / len(df)),
                        "relevant_seen": relevant_seen,
                        "relevant_seen_relative": relevant_seen_rel,
                        "relevant_seen_or_predicted_relative": relevant_seen_or_predicted_rel,
                        "train_recall": recall_score(
                            y_train_true, y_train_pred, zero_division=0  # type: ignore
                        ),
                        "train_precision": precision_score(
                            y_train_true, y_train_pred, zero_division=0  # type: ignore
                        ),
                        "train_f1": f1_score(y_train_true, y_train_pred),
                        "train_all_recall": recall_score(
                            y_train_all_true, y_train_all_pred, zero_division=0  # type: ignore
                        ),
                        "train_all_precision": precision_score(
                            y_train_all_true, y_train_all_pred, zero_division=0  # type: ignore
                        ),
                        "train_all_f1": f1_score(y_train_all_true, y_train_all_pred),
                        "recall": recall_score(
                            y_test_true, y_test_pred, zero_division=0  # type: ignore
                        ),
                        "precision": precision_score(
                            y_test_true, y_test_pred, zero_division=0  # type: ignore
                        ),
                        "f1": f1_score(y_test_true, y_test_pred, zero_division=0),  # type: ignore
                        "work_saved": work_saved,
                        "work_saved_within_target_recall": work_saved
                        if relevant_seen_or_predicted_rel >= TARGET_RECALL
                        else 0,
                        "time_train": training_time,
                        "time_prediction": time() - t1,
                    }
                    evaluation_df = pd.concat(
                        [evaluation_df, pd.DataFrame([evaluation])],
                        ignore_index=True,
                    )
                    evaluation_df.to_csv(f"{folder}/evaluation.csv", index=False)
            clf = None
            gc.collect()
            if torch.cuda.is_available():
                torch.cuda.empty_cache()
    return evaluation_df
