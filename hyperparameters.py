""""
# Hyperparameter search for transformer models

distilroberta-base
learning_rate: float between 1e-6, 1e-4, logarithmic scale
number of epochs: int between 1, 10
batch size: one of 4, 8, 16 (larger batch sizes tend to overload the GPU memory)
all Cohen datasets + partial NETs dataset 
80-20 train-test-split

optimize f1 via optuna, 20 trials per dataset

eventually crashes due to problem with dataset "NETs (partial)"
"""

import pandas as pd
import plotly.express as px
import matplotlib.pyplot as plt


df = pd.DataFrame(
    {
        "Dataset": [
            "UrinaryIncontinence",
            "Antihistamines",
            "Estrogens",
            "NSAIDS",
            "OralHypoglycemics",
            "Triptans",
            "ADHD",
        ],
        "BestRun": ["16", "11", "19", "13", "10", "6", "17"],
        "objective": [
            0.5833333333333334,
            0.7272727272727273,
            0.6428571428571428,
            0.7000000000000001,
            0.6923076923076923,
            0.7083333333333334,
            0.7441860465116279,
        ],
        "learning_rate": [
            2.0686026242589676e-05,
            3.876021411699469e-05,
            5.4183772426697805e-05,
            3.112019708383036e-05,
            8.991071069754968e-05,
            2.032923309919827e-05,
            1.0878127579237014e-05,
        ],
        "num_train_epochs": [10, 7, 10, 4, 8, 6, 7],
        "seed": [29, 16, 16, 22, 38, 11, 39],
        "per_device_train_batch_size": [8, 8, 4, 8, 16, 8, 8],
    }
)

print(df)
df.hist(bins=50, figsize=(20, 15))
plt.show()
