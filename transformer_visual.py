import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns

for attr in ["frozen", "reinit"]:
    classifier_order = [
        "DistilRoBERTa",
        f"DistilRoBERTa {attr}",
    ]

    for metric in ["recall", "f1"]:

        df = pd.read_csv("output-10/evaluation.csv")
        sns.set(font_scale=0.7)
        fig = sns.relplot(
            data=df,
            x="seen_relative",
            y=metric,
            # col="classifier",
            row="dataset",
            hue="query_strategy",
            style="classifier",
            style_order=classifier_order,
            kind="line",
            col_order=classifier_order,
        )
        plt.savefig(f"output-10/transformer-{attr}-{metric}.png")
        plt.close()
