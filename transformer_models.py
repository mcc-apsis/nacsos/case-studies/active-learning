import gc

from datasets import Dataset, load_metric  # type: ignore
import numpy as np
from sklearn.base import BaseEstimator
import tensorflow as tf
import torch
from tqdm import tqdm
from transformers import (
    AutoModelForSequenceClassification,  # type: ignore
    AutoTokenizer,  # type: ignore
    DataCollatorWithPadding,  # type: ignore
    TrainingArguments,  # type: ignore
    Trainer,  # type: ignore
)


def get_abstracts(df):
    return np.array(df["ab"])


def split(l, n):
    for i in range(0, len(l), n):
        yield l[i : i + n]


class CustomTrainer(Trainer):
    def compute_loss(self, model, inputs, return_outputs=False):
        # use a weighted loss (useful when you have an unbalanced training set)
        # copied from https://huggingface.co/docs/transformers/main_classes/trainer
        labels = inputs.get("labels").cpu()
        # forward pass
        outputs = model(**inputs)
        logits = outputs.get("logits").cpu()
        # compute custom loss
        loss_fct = torch.nn.CrossEntropyLoss(weight=torch.tensor([1.0, 1.0]))
        loss = loss_fct(logits.view(-1, self.model.config.num_labels), labels.view(-1))  # type: ignore
        return (loss, outputs) if return_outputs else loss


class TransformerEstimator(BaseEstimator):
    def __init__(self, checkpoint, epochs, balanced, freeze_model=False, reinit=False):
        self.checkpoint = checkpoint
        self.epochs = epochs
        self.balanced = balanced
        self.tokenizer = AutoTokenizer.from_pretrained(checkpoint)
        self.device = "cuda:0" if torch.cuda.is_available() else "cpu"
        self.batch_size = 8
        self.logits = None
        self.freeze_model = freeze_model
        self.reinit = reinit

        def tokenize_function(examples):
            return self.tokenizer(examples["text"], truncation=True)

        self.tokenize_function = tokenize_function
        self.model = AutoModelForSequenceClassification.from_pretrained(
            checkpoint, num_labels=2  # type: ignore
        ).to(self.device)
        self.trainer = None

    def _tokenize_dataset(self, X, y):
        data_dict = {"text": X, "label": y}
        dataset = Dataset.from_dict(data_dict)
        tokenized_dataset = dataset.map(self.tokenize_function, batched=True)
        return tokenized_dataset

    def _initialize_trainer(self, X, y, X_eval=None, y_eval=None):
        train_dataset = self._tokenize_dataset(X, y)
        eval_dataset = (
            self._tokenize_dataset(X_eval, y_eval)
            if X_eval is not None and y_eval is not None
            else None
        )
        data_collator = DataCollatorWithPadding(tokenizer=self.tokenizer)

        def compute_metrics(eval_pred):
            logits, labels = eval_pred
            predictions = np.argmax(logits, axis=-1)
            precision = load_metric("precision").compute(predictions=predictions, references=labels)["precision"]  # type: ignore
            recall = load_metric("recall").compute(predictions=predictions, references=labels)["recall"]  # type: ignore
            f1 = load_metric("f1").compute(predictions=predictions, references=labels)["f1"]  # type: ignore
            return {"precision": precision, "recall": recall, "f1": f1}

        def model_init():
            self.model = None
            gc.collect()
            if torch.cuda.is_available():
                torch.cuda.empty_cache()
            self.model = AutoModelForSequenceClassification.from_pretrained(
                self.checkpoint, num_labels=2  # type: ignore
            ).to(self.device)
            if self.freeze_model:
                # TODO generalize to non-RoBERTa models
                for param in self.model.roberta.parameters():
                    param.requires_grad = False
            if self.reinit:
                # copied and modified from
                # https://github.com/asappresearch/revisit-bert-finetuning/blob/master/run_glue.py
                # see https://arxiv.org/abs/2006.05987
                encoder_temp = self.model.roberta
                # reinit pooler
                if encoder_temp.pooler is not None:
                    encoder_temp.pooler.dense.weight.data.normal_(
                        mean=0.0, std=encoder_temp.config.initializer_range
                    )
                    encoder_temp.pooler.dense.bias.data.zero_()
                    for p in encoder_temp.pooler.parameters():
                        p.requires_grad = True

                # reinit layers

                PARAM = 5  # = number of reinitialized layers? TODO
                for layer in encoder_temp.encoder.layer[-PARAM:]:
                    for module in layer.modules():
                        if isinstance(module, (torch.nn.Linear, torch.nn.Embedding)):
                            # Slightly different from the TF version which uses truncated_normal for initialization
                            # cf https://github.com/pytorch/pytorch/pull/5617
                            module.weight.data.normal_(
                                mean=0.0, std=encoder_temp.config.initializer_range
                            )
                        elif isinstance(module, torch.nn.LayerNorm):
                            module.bias.data.zero_()
                            module.weight.data.fill_(1.0)
                        if (
                            isinstance(module, torch.nn.Linear)
                            and module.bias is not None
                        ):
                            module.bias.data.zero_()
            return self.model

        training_args = TrainingArguments(
            output_dir=f"./output/{self.checkpoint}",
            learning_rate=2e-5,
            per_device_train_batch_size=self.batch_size,
            per_device_eval_batch_size=self.batch_size,
            num_train_epochs=self.epochs,
            weight_decay=0.01,
            evaluation_strategy="epoch" if eval_dataset is not None else "no",  # type: ignore
        )
        trainer = CustomTrainer if self.balanced else Trainer
        self.trainer = trainer(
            args=training_args,
            train_dataset=train_dataset,
            eval_dataset=eval_dataset,
            tokenizer=self.tokenizer,
            data_collator=data_collator,
            compute_metrics=compute_metrics,
            model_init=model_init,
        )

    def fit(self, X, y, X_eval=None, y_eval=None):
        self._initialize_trainer(X, y, X_eval, y_eval)
        self.trainer.train()  # type: ignore

    def predict(self, X):
        self.logits = None
        for X_ in tqdm(list(split(X, self.batch_size))):
            encodings = self.tokenizer(
                list(X_),
                padding="max_length",
                truncation=True,
                return_tensors="pt",
            ).to(self.device)
            output = self.model(**encodings)
            logits = output.logits.cpu().detach().numpy()
            self.logits = (
                np.concatenate([self.logits, logits])
                if self.logits is not None
                else logits
            )
        predicted_class_ids = tf.argmax(self.logits, axis=-1)
        return predicted_class_ids

    def predict_proba(self, X):
        self.predict(X)
        return tf.math.softmax(self.logits, axis=-1)  # type: ignore

    def hyperparameter_search(self, X, y, X_eval, y_eval):
        self._initialize_trainer(X, y, X_eval, y_eval)

        def default_hp_space_optuna(trial):
            return {
                "learning_rate": trial.suggest_float(
                    "learning_rate", 1e-6, 1e-4, log=True
                ),
                "num_train_epochs": trial.suggest_int("num_train_epochs", 1, 10),
                "seed": trial.suggest_int("seed", 1, 40),
                "per_device_train_batch_size": trial.suggest_categorical(
                    "per_device_train_batch_size", [4, 8, 16]
                ),
            }

        best_trial = self.trainer.hyperparameter_search(  # type: ignore
            backend="optuna",
            compute_objective=lambda d: d["eval_f1"],
            direction="maximize",
            hp_space=default_hp_space_optuna,
            n_trials=20,
        )
        return best_trial
